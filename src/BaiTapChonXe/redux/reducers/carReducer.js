import { CHOOSE_CAR } from "../constants/constant";

let initialState = {
  selectedCar: "./img/car/red-car.jpg",

  imgArr: [
    "./img/car/red-car.jpg",
    "./img/car/black-car.jpg",
    "./img/car/silver-car.jpg",
  ],
};

export const carReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case CHOOSE_CAR:
      return {
        ...state,
        selectedCar: payload,
      };

    default:
      return state;
  }
};
