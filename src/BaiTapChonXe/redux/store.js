import { createStore } from "redux";
import { rootReducer } from "./reducers/rootReducers";

export const storechooseCar = createStore(
  rootReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
