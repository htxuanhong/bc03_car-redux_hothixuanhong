import React, { Component } from "react";
import { connect } from "react-redux";
import { CHOOSE_CAR } from "./redux/constants/constant";

class ProductItem extends Component {
  render() {
    return (
      <div className="container py-4">
        <div>
          <img className="w-75 h-75" src={this.props.selectedCar} alt="" />
        </div>
        <div>
          <h4 className="text-info pt-4"> Chọn xe mà bạn thích!!!!!!</h4>

          {this.props.imgArr.map((url, index) => {
            return (
              <img
                key={index}
                style={{ width: "150px", margin: "30px" }}
                src={url}
                alt=""
                className={
                  this.props.selectedCar === url ? "border border-success" : ""
                }
                onClick={() => {
                  this.props.handleChangeCar(url);
                }}
              />
            );
          })}
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    imgArr: state.carReducer.imgArr,
    selectedCar: state.carReducer.selectedCar,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleChangeCar: (url) => {
      dispatch({
        type: CHOOSE_CAR,
        payload: url,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductItem);
