import "antd/dist/antd.css";
import "./App.css";
import BaiTapChonXe from "./BaiTapChonXe/BaiTapChonXe";

function App() {
  return (
    <div className="App">
      <BaiTapChonXe />
    </div>
  );
}

export default App;
